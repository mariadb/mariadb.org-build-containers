#!/bin/bash
#
# This script expects 4 arguments:
# * MariaDB Server branch name
# * Target environment Docker name
# * Target environment Docker tag
# * Docker registry name
#
# Example: ./build.sh 10.3 ubuntu 16.04 registry.gitlab.com/mariadb/mariadb.org-build-containers

set -x
set -e

NAME="mariadb-$1-$2-$3-build-env"

cp Dockerfile.template Dockerfile.$NAME
sed "s/10.3/$1/g" -i Dockerfile.$NAME
sed "s/ubuntu/$2/g" -i Dockerfile.$NAME
sed "s/16.04/$3/g" -i Dockerfile.$NAME

# Use --cache-from for quick rebuilds
#docker pull $4:$NAME || true
#docker build --cache-from $4:$NAME --tag $4:$NAME --file Dockerfile.$NAME .

# Force --pull to make sure the base images are built of latest version
docker build --pull --tag $4:$NAME --file Dockerfile.$NAME .

docker push $4:$NAME

