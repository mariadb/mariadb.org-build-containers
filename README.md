## Development containers for MariaDB

Trying to make the CI builds for MariaDB run faster with a Docker overlay.

For each CI build we were using a base OS image, and then installing all of the dependencies that we needed on it. By using a Docker overlay we can install those dependencies once, and then overlay it for each build. This speeds up our CI builds (as there can be several in a pipeline) and reduces the amount of text in the logs.

These images end up in our development [container registry of the mariadb.org-build-containers project](https://gitlab.com/mariadb/mariadb.org-build-containers/container_registry). You can use them with this:

```bash
docker pull registry.gitlab.com/mariadb/mariadb.org-build-containers:mariadb-10.3-ubuntu-16.04-build-env
```

The images are automatically rebuilt on the first day of every month or when someone changes the Dockerfile in this repo.

***NOTE!*** These images are for developers of MariaDB. These are not in any way general purpose MariaDB images.


### Protected branch testing

This repository also has the following settings and supporting .gitlab-ci.yml in place:
- Pushing to master is completely forbidden. Commits on master can only land via merge requests or via fire-and-forget merges done by CI (or manually using special SSH keys).
- Fire-and-forget merges: All commits pushed on staging will be merged on master automatically if CI passed.
- Merges are only allowed as fast-forwards (only linear history, no merge commits allowed).
- Merges are only allowed if CI passed.
- Commits are only allowed using proper e-mail addresses.
- New Docker images will be built and published automatically on every push.
- New Docker images will also be built on the 1st day of every month automatically.


Relevant settings pages:
- Push rules, Protected branches and Deploy keys: Settings/repository
- Merge request settings: Setting/General
- Secret variables: Settings/CI
- Monthly rebuild: CI/CD / Schedules
